/*
 * Manual.h
 *
 *  Created on: 6 ene. 2022
 *      Author: marti
 */

#ifndef SRC_MANUAL_H_
#define SRC_MANUAL_H_

#include <Estados.h>





class Manual: public Estados {
	friend class Estados;

		Manual();
public:

	virtual ~Manual();
	virtual uint8_t execute(void* valor=0);
};

#endif /* SRC_MANUAL_H_ */
