/*
 * Acel.h
 *
 *  Created on: 6 ene. 2022
 *      Author: marti
 */

#ifndef SRC_ACEL_H_
#define SRC_ACEL_H_

#include <Estados.h>

class Acel: public Estados {
private:
	friend class Estados;
public:
	Acel();
	virtual uint8_t execute(void * valor=0);
	virtual ~Acel();
};

#endif /* SRC_ACEL_H_ */
