/*
 * Estados.h
 *
 *  Created on: 6 ene. 2022
 *      Author: marti
 */

#ifndef ESTADOS_H_
#define ESTADOS_H_

#include "stm32f4xx_hal.h"
#include "math.h"
#include <string>
#include <vector>

// MACROS
#define TIM_CHANNEL_DIFF 0x00000004

// EXTERN HANDLERS
extern ADC_HandleTypeDef hadc1;

extern I2C_HandleTypeDef hi2c1;

extern SPI_HandleTypeDef hspi1;

extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;

// For the callback
extern volatile int8_t Encoder;

// flag for the encoder
extern volatile uint8_t ISR_flag_Encoder;
extern int Encoder_debouncer(volatile uint8_t* button_int, TIM_HandleTypeDef *htim, uint16_t diff);

// EXTERN SPI BUFFERS Variables;
extern uint8_t spiRxBuf[2];
extern uint8_t spiTxBuf[2];

// EXTERN LEDS VALUES FOR PWM
extern int  led[7];

// DECLARATIONS OF FUNCTIONS


// DECLARATIONS OF DERIVATED CLASSES
class Acel;
class Automatic;
class Manual;
class Show;

using namespace std;
enum name {MANUAL,ACEL,AUTO,SHOW};

class Estados {

protected:
	string name;

public:
	Estados(string name);
	virtual ~Estados();

	virtual string getName(){return name;}
	virtual uint8_t execute(void * valor=0)=0;

	static Estados* Fabricador(enum name);
};

#endif /* ESTADOS_H_ */
