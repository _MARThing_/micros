/*
 * Menu.h
 *
 *  Created on: Jan 5, 2022
 *      Author: marti
 */

#ifndef SRC_MENU_H_
#define SRC_MENU_H_

#include "Estados.h"
#include <vector>

using namespace std;

class Menu {
	std::vector<Estados*> states;
	uint8_t option;
	uint8_t pressed_button;// Push-down resistor
public:
	Menu();
	virtual ~Menu();
	string getStateName(); // Devuelve el nombre del estado que se encuentra
	void setOp(uint8_t number); // Asignación del valor de la opción seleccionada
	void setButtonState(uint8_t number); //Asignación sí se ha seleccionado el estado o no, para su ejecución
	uint8_t execute(void * valor=0); // Ejecuta la acción de el estado que se encuentra
};

#endif /* SRC_MENU_H_ */
