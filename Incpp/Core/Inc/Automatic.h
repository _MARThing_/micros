/*
 * Automatic.h
 *
 *  Created on: 6 ene. 2022
 *      Author: marti
 */

#ifndef SRC_AUTOMATIC_H_
#define SRC_AUTOMATIC_H_

#include <Estados.h>

class Automatic: public Estados{
	friend class Estados;
public:
	Automatic();
	virtual uint8_t execute(void* valor=0);
	virtual ~Automatic();
};

#endif /* SRC_AUTOMATIC_H_ */
