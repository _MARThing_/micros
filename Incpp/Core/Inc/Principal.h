
#ifndef _PRINCIPAL_
#define _PRINCIPAL_

#include "stm32f4xx_hal.h"
#include "fonts.h"
#include "ssd1306.h"
#include "math.h"

/*Returns true when the button has been pressed after debounce period*/
 int debouncer(volatile uint8_t* button_int, GPIO_TypeDef* GPIO_port, uint16_t GPIO_number){
 	static uint8_t button_count=0;
 	static int counter=0;

 	if (*button_int==1){
 		if (button_count==0) {
 			counter=HAL_GetTick();
 			button_count++;
 		}
 		if (HAL_GetTick()-counter>=20){
 			counter=HAL_GetTick();
 			if (HAL_GPIO_ReadPin(GPIO_port, GPIO_number)!=1){
 				button_count=1;
 			}
 			else{
 				button_count++;
 			}
 			if (button_count==4){ //Periodo antirebotes
 				button_count=0;
 				*button_int=0;
 				return 1;
 			}
 		}
 	}
 	return 0;
 }

// Debouncer for the Encoder, it returns a 1 when there is a stable rotation
int Encoder_debouncer(volatile uint8_t* button_int, TIM_HandleTypeDef *htim, uint16_t diff){
 	static uint8_t button_count=0;
 	static int counter=0;
 	static uint32_t Pre= 0;

 	if (*button_int==1){
 		if (button_count==0) {
 			counter=HAL_GetTick();
 			button_count++;
 		}
 		if (HAL_GetTick()-counter>=20){
 			counter=HAL_GetTick();
 			if (abs( (int) (__HAL_TIM_GET_COUNTER(htim)-Pre) ) < diff){
 				button_count=1;
 			}
 			else{
 				button_count++;
 			}
 			if (button_count==4){ //Periodo antirebotes
 				button_count=0;
 				*button_int=0;
 				Pre=__HAL_TIM_GET_COUNTER(htim);
 				return 1;
 			}
 		}
 	}

 	return 0;
 }

// Clears every Interruption that is pending on a GPIO Pin
 void ClearPendingIRQ(uint16_t GPIO_Pin, IRQn_Type handler ){
	   while(HAL_NVIC_GetPendingIRQ(handler)){
	    	__HAL_GPIO_EXTI_CLEAR_IT(GPIO_Pin);
	    	HAL_NVIC_ClearPendingIRQ(handler);
	    }
 }

// Function that depending on the state it prints its name on the screen
void print_OLED(uint8_t estado,uint8_t button_state){

	  switch(estado){

	  case 0:
		  SSD1306_Clear();
		  SSD1306_GotoXY (10,0);
		  SSD1306_Puts((char*) "1. MANUAL", &Font_11x18, SSD1306_COLOR_WHITE);
		  /*SSD1306_GotoXY(30, 30);
		  SSD1306_Puts((char*) "Rotate!", &Font_11x18, SSD1306_COLOR_WHITE);
		  SSD1306_DrawCircle(64, 40, 23, SSD1306_COLOR_WHITE);
		  SSD1306_UpdateScreen();*/

		  break;
	  case 1:
		  SSD1306_Clear();
		  SSD1306_GotoXY (3,0);
		  SSD1306_Puts((char*) "2. ACELERA", &Font_11x18, SSD1306_COLOR_WHITE);
		  /*SSD1306_GotoXY(25, 30);
		  SSD1306_Puts((char*) "Move it!", &Font_11x18, SSD1306_COLOR_WHITE);
		  SSD1306_DrawRectangle(0, 20, 128, 44, SSD1306_COLOR_WHITE);
		  SSD1306_UpdateScreen();*/


		  break;

	  case 2:
		  SSD1306_Clear();
		  SSD1306_GotoXY (20,0);
		  SSD1306_Puts((char*) "3. AUTO", &Font_11x18, SSD1306_COLOR_WHITE);
		  /*SSD1306_GotoXY(35, 30);
		  SSD1306_Puts((char*) "Shine!", &Font_11x18, SSD1306_COLOR_WHITE);
		  SSD1306_DrawCircle(64, 40, 23, SSD1306_COLOR_WHITE);
		  SSD1306_UpdateScreen();*/


		  break;

	  case 3:
		  SSD1306_Clear();
		  SSD1306_GotoXY (25,0);
		  SSD1306_Puts((char*) "4. SHOW", &Font_11x18, SSD1306_COLOR_WHITE);
		  /*SSD1306_GotoXY(35, 30);
		  SSD1306_Puts((char*) "ENJOY!", &Font_11x18, SSD1306_COLOR_WHITE);
		  SSD1306_DrawRectangle(0, 20, 128, 44, SSD1306_COLOR_WHITE);
		  SSD1306_UpdateScreen();*/

		  break;

	  }
	  if(button_state){
		  SSD1306_GotoXY(30, 30);
		  SSD1306_Puts((char*) "Running", &Font_11x18, SSD1306_COLOR_WHITE);
	  }
	  else{
		  SSD1306_GotoXY(30, 30);
		  SSD1306_Puts((char*) "Menu", &Font_11x18, SSD1306_COLOR_WHITE);
	  }

	  SSD1306_DrawRectangle(0, 20, 128, 44, SSD1306_COLOR_WHITE);
	  SSD1306_UpdateScreen();
}

#endif /*_PRINCIPAL*/
