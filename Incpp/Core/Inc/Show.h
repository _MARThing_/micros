/*
 * Show.h
 *
 *  Created on: 6 ene. 2022
 *      Author: marti
 */

#ifndef SRC_SHOW_H_
#define SRC_SHOW_H_

#include <Estados.h>

class Show: public Estados {
	friend class Estados;
public:
	Show();
	virtual uint8_t execute(void* valor=0);
	virtual ~Show();
};

#endif /* SRC_SHOW_H_ */
