/*
 * Automatic.cpp
 *
 *  Created on: 6 ene. 2022
 *      Author: marti
 */

#include "Automatic.h"

Automatic::Automatic():Estados("Modo Automatico") {
	// TODO Auto-generated constructor stub

}

Automatic::~Automatic() {
	// TODO Auto-generated destructor stub
}

uint8_t Automatic::execute(void* valor){

	int ADC_val;
	//while(valor==2){
	/*Convierte valor analógico de iluminación en digital*/
	HAL_ADC_Start(&hadc1);
	if(HAL_ADC_PollForConversion(&hadc1,1000)==HAL_OK){
		ADC_val=HAL_ADC_GetValue(&hadc1);
	}
	else{
		return 1;
	}
	HAL_ADC_Stop(&hadc1);

	/*En función de ADC_value, calcula valores de led*/
	led[0]=625-8*ADC_val;
	for(int i=1;i<7;i++){
		led[i]=led[i-1]+100;
	}
	/*Si valor de led<0 --> led=0*/
	for(int i=0;i<7;i++){
		if(led[i]<0)
			led[i]=0;
	}

	/*PWM signal generation*/
		for(int i=0;i<4;i++){
			__HAL_TIM_SET_COMPARE(&htim2,(TIM_CHANNEL_1 | TIM_CHANNEL_DIFF*i)  , led[i]);
		}
		for(int i=0;i<3;i++){
			__HAL_TIM_SET_COMPARE(&htim3,(TIM_CHANNEL_1 | TIM_CHANNEL_DIFF*i), led[i+4]);
		}

//}


	return 0;
}
