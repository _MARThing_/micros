/*
 * Show.cpp
 *
 *  Created on: 6 ene. 2022
 *      Author: marti
 */

#include "Show.h"

Show::Show():Estados("Show") {
	// TODO Auto-generated constructor stub

}

Show::~Show() {
	// TODO Auto-generated destructor stub
}

void setPWM_Delay(TIM_HandleTypeDef* htim,uint8_t n_channels, uint16_t value){
	for(int i=0; i<n_channels;i++){
		__HAL_TIM_SET_COMPARE(htim,(TIM_CHANNEL_1 | TIM_CHANNEL_DIFF*i)  , value);
		HAL_Delay(100);
	}
}

uint8_t Show::execute(void* valor){

/*
#define TIM_CHANNEL_1                      0x00000000U
#define TIM_CHANNEL_2                      0x00000004U
#define TIM_CHANNEL_3                      0x00000008U
#define TIM_CHANNEL_4                      0x0000000CU
#define TIM_CHANNEL_ALL                    0x0000003CU
 */
	//Valor random numero de Espectaculo
	uint16_t nEspect;
	nEspect=rand()%20;
	//Espectaculo 1
	if(nEspect<10){

		setPWM_Delay(&htim2,4,625);
		setPWM_Delay(&htim3,3,625);
		setPWM_Delay(&htim2,4,0);
		setPWM_Delay(&htim3,3,0);
		/*
		__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_1, 625);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_2, 625);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_3, 625);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_4, 625);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim3,TIM_CHANNEL_1, 625);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim3,TIM_CHANNEL_2, 625);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim3,TIM_CHANNEL_3, 625);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_1, 0);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_2, 0);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_3, 0);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_4, 0);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim3,TIM_CHANNEL_1, 0);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim3,TIM_CHANNEL_2, 0);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim3,TIM_CHANNEL_3, 0);
		HAL_Delay(100);*/


	}
	//Espectaculo 2
	else if(nEspect>=10){

		__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_1, 625);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_3, 625);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim3,TIM_CHANNEL_1, 625);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim3,TIM_CHANNEL_3, 625);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_1, 0);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_3, 0);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim3,TIM_CHANNEL_1, 0);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim3,TIM_CHANNEL_3, 0);
		HAL_Delay(100);


		__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_2, 625);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_4, 625);
		HAL_Delay(100);
		__HAL_TIM_SET_COMPARE(&htim3,TIM_CHANNEL_2, 625);
		HAL_Delay(100);


		__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_2, 0);
		HAL_Delay(100);


		__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_4, 0);
		HAL_Delay(100);


		__HAL_TIM_SET_COMPARE(&htim3,TIM_CHANNEL_2, 0);
		HAL_Delay(100);

	}


	return 0;
}
