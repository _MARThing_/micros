/*
 * Menu.cpp
 *
 *  Created on: Jan 5, 2022
 *      Author: marti
 */

#include "Menu.h"

Menu::Menu(): option(0),pressed_button(0) {
	// TODO Auto-generated constructor stub
	states.push_back(Estados::Fabricador(MANUAL));
	states.push_back(Estados::Fabricador(ACEL));
	states.push_back(Estados::Fabricador(AUTO));
	states.push_back(Estados::Fabricador(SHOW));

}

Menu::~Menu() {
	// TODO Auto-generated destructor stub

	// Borrar todos los posibles estados
	for(auto i:states)
		delete i;
	states.clear();

}

string Menu::getStateName(){
	return states.size()>0 ? states[option]->getName(): "ERROR";
}

void Menu::setOp(uint8_t number){
	option= number<states.size() ? number:0;
}

void Menu::setButtonState(uint8_t number){
	pressed_button=number;
}

uint8_t Menu::execute(void * valor){
	return pressed_button ? states[option]->execute(valor):-1;
}











