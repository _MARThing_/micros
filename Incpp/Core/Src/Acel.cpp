/*
 * Acel.cpp
 *
 *  Created on: 6 ene. 2022
 *      Author: marti
 */

#include "Acel.h"

Acel::Acel():Estados("Modo Inclinometro") {
	// TODO Auto-generated constructor stub

}

Acel::~Acel() {
	// TODO Auto-generated destructor stub
}


uint8_t Acel::execute(void* valor){

	// Variables
	uint8_t auxiliar_acc;
	int8_t accel_x,accel_y,accel_z; // Accelerations


	//while(valor==1){

	// READ THE X AXE
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_3,GPIO_PIN_RESET);
	spiTxBuf[0]=0x29|0x80; //Enable Read Mode
	HAL_SPI_Transmit(&hspi1,spiTxBuf,1,HAL_MAX_DELAY);
	/*Recibir datos*/
	HAL_SPI_Receive(&hspi1,&auxiliar_acc,1,HAL_MAX_DELAY);
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_3,GPIO_PIN_SET);
	accel_x=(int8_t) auxiliar_acc;
	HAL_Delay(100);


	// READ THE Y AXE
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_3,GPIO_PIN_RESET);
	spiTxBuf[0]=0x2B|0x80; //Enable Read Mode
	HAL_SPI_Transmit(&hspi1,spiTxBuf,1,HAL_MAX_DELAY);
	/*Recibir datos*/
	HAL_SPI_Receive(&hspi1,&auxiliar_acc,1,HAL_MAX_DELAY);
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_3,GPIO_PIN_SET);
	accel_y=(int8_t) auxiliar_acc;
	HAL_Delay(100);


	// READ THE Z AXE
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_3,GPIO_PIN_RESET);
	spiTxBuf[0]=0x2D|0x80; //Enable Read Mode
	HAL_SPI_Transmit(&hspi1,spiTxBuf,1,HAL_MAX_DELAY);
	/*Recibir datos*/
	HAL_SPI_Receive(&hspi1,&auxiliar_acc,1,HAL_MAX_DELAY);
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_3,GPIO_PIN_SET);
	accel_z=(int8_t) auxiliar_acc;

	HAL_Delay(100);


	////////////////////////////////////////////////////////
	/*En funcion de los valores de las aceleraciones, damos valores a leds*/
	if(accel_x>8 || accel_x<-8){ led[0]=accel_x*5;led[1]=accel_x*5;}
	else {led[0]=0;led[1]=0;}
	if(accel_y>8||accel_y<-8) { led[2]=accel_y*5;led[3]=accel_y*5;}
	else {led[2]=0;led[3]=0;}
	if(accel_z>70||accel_z<-70){ led[4]=accel_z*3;led[5]=accel_z*3;led[6]=accel_z*3;}
	else {led[4]=0;led[5]=0;led[6]=0;}



	/*PWM signal generation*/
	for(int i=0;i<4;i++){
		__HAL_TIM_SET_COMPARE(&htim2,(TIM_CHANNEL_1 | TIM_CHANNEL_DIFF*i)  , led[i]);
	}
	for(int i=0;i<3;i++){
		__HAL_TIM_SET_COMPARE(&htim3,(TIM_CHANNEL_1 | TIM_CHANNEL_DIFF*i), led[i+4]);
	}

	//}
	/*
	uint16_t* a=(uint16_t*) valor;
	__HAL_TIM_SET_COMPARE(handler_tim,TIM_CHANNEL_1,(*a)*9);*/
	return 0;
}
