/*
 * Manual.cpp
 *
 *  Created on: 6 ene. 2022
 *      Author: marti
 */

#include "Manual.h"

Manual::Manual():Estados("Control Manual") {
	// TODO Auto-generated constructor stub

}

Manual::~Manual() {
	// TODO Auto-generated destructor stub
}

// Auxiliar function that sets the PWM value of a timer of a number of channels.
// It starts in channel 1 and goes up.
void setPWM(TIM_HandleTypeDef* htim,uint8_t n_channels, uint16_t value){
	for(int i=0; i<n_channels;i++){
		__HAL_TIM_SET_COMPARE(htim,(TIM_CHANNEL_1 | TIM_CHANNEL_DIFF*i)  , value);
	}
}

// This function takes the encoder relative position and after treating it
// It sets a value to the LEDs using PWM
uint8_t Manual::execute(void* valor){

	int16_t encoder_position=(int16_t) __HAL_TIM_GET_COUNTER(&htim1);
	static uint16_t counter=0;
 	static int16_t encoder_PrePosition=encoder_position;
	
	// ADJUST THE PWM VALUES
	if(ISR_flag_Encoder){
	 // Range of values of PWM 0-625
		int16_t difference= encoder_position-encoder_PrePosition;

	  // Change the actual state
	  if(difference>2)
		  counter+=100;
	  else if(difference<0)
		  counter-=100;
		  
	  counter= counter>6240 ? 0:counter;
	  counter= counter>624 ? 624:counter;
	  
	   ISR_flag_Encoder=0;
	  encoder_PrePosition=encoder_position;
	}
	
	// Set the PWM values
	 setPWM(&htim2, 4, counter);
	 setPWM(&htim3,3,counter);

	return 0;
}
